module.exports = {
  pwa: {
    name: '(RE)Sources Relationnelles',
    themeColor: '#00898E',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
  }
}
