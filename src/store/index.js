import { createStore } from 'vuex';
import newsFeedModule from '@/store/modules/newsfeed';
import userModule from '@/store/modules/user';
import resourceModule from '@/store/modules/resource';

const store = createStore({
  modules: {
    user: userModule,
    newsFeed: newsFeedModule,
    resource: resourceModule,
  },
});

export default store;
