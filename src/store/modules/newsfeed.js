import newsfeedService from '@/services/newsfeed.service';

const newsFeedModule = {
  state: {
    newsFeed: null,
    isFetchingNewsFeed: false,
  },
  mutations: {
    fetchNewsFeedRequest(state) {
      state.newsFeed = null;
      state.isFetchingNewsFeed = true;
    },
    fetchNewsFeedSuccess(state, newsFeed) {
      state.newsFeed = newsFeed;
      state.isFetchingNewsFeed = false;
    },
    fetchNewsFeedFailure(state) {
      state.newsFeed = null;
      state.isFetchingNewsFeed = false;
    },
  },
  actions: {
    async fetchNewsFeed({ commit }) {
      await commit('fetchNewsFeedRequest');

      try {
        const response = await newsfeedService.fetchNewsFeed();
        await commit('fetchNewsFeedSuccess', response.data);
      } catch (error) {
        await commit('fetchNewsFeedFailure');
      }
    },
  },
};

export default newsFeedModule;
