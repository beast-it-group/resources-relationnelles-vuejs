export default {
  article: 'Article', // newspaper
  activity: 'Activité/Jeu à réaliser', // puzzle
  online_game: 'Image', // desktop-computer
  workshop: 'Exercice/Atelier', // beaker
  challenge_card: 'Carte défi', // light-bulb
  video: 'Vidéo', // video-camera
  pdf_course: 'Cours au format PDF', // presentation-chart-line
  reading_note: 'Fiche de lecture', // document-text
};
