export default async (to, from, next) => {
  await this.$store.dispatch('checkIfUserTokenExists');
  if (!this.$store.getters.isLoggedIn) {
    return next({ name: 'LoginView' });
  }
  return next();
};
