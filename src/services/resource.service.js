import apiService from '@/services/api.service';

export default {
  async createResource(data, userToken) {
    const url = 'resources';

    const formData = new FormData();
    Object.keys(data).forEach((key) => {
      formData.append(key, data[key]);
    });

    return await apiService.postMultipart(url, formData, userToken);
  },
};
