import apiService from '@/services/api.service';

export default {
  async fetchNewsFeed() {
    const url = 'groups/1/newsfeed';
    return await apiService.get(url);
  },
};
