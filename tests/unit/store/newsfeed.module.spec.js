import newsfeedService from "@/services/newsfeed.service";
import newsFeedModule from "@/store/modules/newsfeed";

describe('Given the user needs the newsfeed', () => {
  test('When calling fetchNewsFeed, newsFeedService is called', async () => {
    const spy = jest.spyOn(newsfeedService, 'fetchNewsFeed').mockImplementation(() => jest.fn());
    const commit = jest.fn();

    await newsFeedModule.actions.fetchNewsFeed({commit});

    expect(spy).toHaveBeenCalled();
    spy.mockRestore();
  });
})
