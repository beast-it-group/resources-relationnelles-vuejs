import { mount } from '@vue/test-utils';
import TheHeader from "@/layout/TheHeader.vue";

describe('Given the user is not logged in', () => {
  test('When loading the header, then \"userIsLogged\" is set to false', () => {
    const $store = {
      getters: {
        isLoggedIn: false,
      }
    };

    const wrapper = mount(TheHeader, {
      global: {
        mocks: {
          $store,
        },
      },
      shallow: true,
    });

    expect(wrapper.vm.userIsLogged).toBeFalsy();
  });
});

describe('Given the user is logged in', () => {
  test('When loading the header, then \"userIsLogged\" is set to true', () => {
    const $store = {
      getters: {
        isLoggedIn: true,
      }
    };

    const wrapper = mount(TheHeader, {
      global: {
        mocks: {
          $store,
        },
      },
      shallow: true,
    });

    expect(wrapper.vm.userIsLogged).toBeTruthy();
  });
});
