import { mount } from '@vue/test-utils';
import TheDesktopMenu from '@/layout/Desktop/TheDesktopMenu';

describe('Given the user is logged in', () => {
  test('When accessing any page, the header displays his avatar', () => {
    const wrapper = mount(TheDesktopMenu, {
      props: {
        userIsLogged: true,
      },
    });
    expect(wrapper.find('#user-btn').text()).toBe('');
  });
});

describe('Given the user is not logged in', () => {
  test('When accessing any page, the header displays the login button', () => {
    const wrapper = mount(TheDesktopMenu, {
      props: {
        userIsLogged: false,
      },
    });

    expect(wrapper.find('#user-btn').text()).toBe('Se connecter');
  });
});
