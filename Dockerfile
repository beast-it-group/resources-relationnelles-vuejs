ARG NODE_BASE_IMAGE="node:18-alpine"
ARG NGINX_BASE_IMAGE="nginx:1.21-alpine"

FROM ${NODE_BASE_IMAGE} as build

WORKDIR /app

COPY package*.json ./
COPY yarn.lock ./
RUN yarn install
COPY . .
RUN yarn build

FROM ${NGINX_BASE_IMAGE} as production

COPY --from=build /app/dist /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
